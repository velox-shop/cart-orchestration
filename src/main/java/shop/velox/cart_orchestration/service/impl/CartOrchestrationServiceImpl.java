package shop.velox.cart_orchestration.service.impl;

import static org.apache.commons.lang3.ObjectUtils.firstNonNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Supplier;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.core5.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.availability.api.dto.AvailabilityDto;
import shop.velox.availability.model.AvailabilityStatus;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.cart_orchestration.api.dto.ItemDto;
import shop.velox.cart_orchestration.api.dto.MessageDto;
import shop.velox.cart_orchestration.api.dto.MessageLevel;
import shop.velox.cart_orchestration.api.dto.RestResponsePage;
import shop.velox.cart_orchestration.service.CartOrchestrationService;
import shop.velox.catalog_orchestration.api.dto.EnrichedProductDto;
import shop.velox.price.api.dto.CurrencyDto;
import shop.velox.price.api.dto.PriceDto;

@Service
public class CartOrchestrationServiceImpl implements CartOrchestrationService {

  private static final Logger LOG = LoggerFactory.getLogger(CartOrchestrationServiceImpl.class);

  private final RestTemplate restTemplate;

  private final ResourceBundleMessageSource messageSource;

  @Value("${cart.url}")
  private String cartUrl;

  @Value("${price.url}")
  private String priceUrl;

  @Value("${availability.url}")
  private String availabilityUrl;

  @Value("${catalog.url}")
  private String catalogUrl;

  @Autowired
  public CartOrchestrationServiceImpl(final RestTemplate restTemplate,
      final ResourceBundleMessageSource messageSource) {
    this.restTemplate = restTemplate;
    this.messageSource = messageSource;
  }

  @Override
  public ResponseEntity<CartDto> createCart(shop.velox.cart.api.dto.CartDto cartServiceDto) {
    verifyCurrency(cartServiceDto);

    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts")
        .build()
        .toUri();
    ResponseEntity<CartDto> cartEntity = restTemplate.postForEntity(uri, cartServiceDto,
        CartDto.class);

    return ResponseEntity.status(cartEntity.getStatusCode()).body(cartEntity.getBody());
  }

  @Override
  public ResponseEntity<CartDto> getCart(final String cartId, final String userId,
      final List<String> refresh) {

    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cartId)
        .build()
        .toUri();
    ResponseEntity<CartDto> cartResponseEntity = restTemplate.getForEntity(uri, CartDto.class);
    CartDto cartDto = cartResponseEntity.getBody();
    LOG.debug("getCart status: {},  body: {}", cartResponseEntity.getStatusCode(), cartDto);

    if (HttpStatus.NOT_FOUND.equals(cartResponseEntity.getStatusCode())) {
      return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
    }

    //Status code FORBIDDEN (403) can occur if the cart does not have an owner and the user is authenticated or if the cart is already owned by someone
    if (HttpStatus.FORBIDDEN.equals(cartResponseEntity.getStatusCode()) || (
        StringUtils.isBlank(cartDto.getUserId()) && StringUtils.isNotBlank(userId))) {
      //try to set current authenticated user as a cart owner
      ResponseEntity<CartDto> notAnonymousCartResponseEntity = acquireCart(cartId, cartDto);

      //if the cart cannot be acquired, it already has an owner
      if (HttpStatus.FORBIDDEN.equals(notAnonymousCartResponseEntity.getStatusCode())) {
        return new ResponseEntity<>(HttpStatus.FORBIDDEN);
      }
      //the user has successfully obtained the cart
      else {
        LOG.error("Failed to acquire cart: {}", cartDto);
        return notAnonymousCartResponseEntity;
      }
    }

    //the user with expired token or wrong credentials tries to get the cart
    if (HttpStatus.UNAUTHORIZED.equals(cartResponseEntity.getStatusCode())) {
      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
    }

    if (!cartResponseEntity.getStatusCode().is2xxSuccessful()) {
      return new ResponseEntity<>(cartResponseEntity.getStatusCode());
    }
    LOG.warn("Get Cart for cartId: {} returns {}", cartId, cartResponseEntity.getStatusCode());
    return ResponseEntity.status(cartResponseEntity.getStatusCode())
        .body(enrichCart(cartDto, refresh));
  }

  public ResponseEntity<CartDto> acquireCart(final String cartId, final CartDto cartDto) {
    LOG.info("Trying to acquire cart: {} belonging to: {}", cartId, cartDto.getUserId());
    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cartId)
        .build()
        .toUri();
    return restTemplate.exchange(uri, HttpMethod.PATCH, new HttpEntity<>(cartDto), CartDto.class);
  }

  @Override
  public ResponseEntity<Void> removeCart(final String cartId) {
    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cartId)
        .build()
        .toUri();
    ResponseEntity<Void> responseEntity = restTemplate
        .exchange(uri, HttpMethod.DELETE, null, Void.class);

    if (responseEntity.getStatusCode().equals(HttpStatus.NOT_FOUND)) {
      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
    LOG.debug("delete responseEntity: {}", responseEntity);
    return responseEntity;
  }

  protected CartDto enrichCart(final CartDto cartDto, final List<String> refresh) {
    LOG.debug("Enriching cart {} with refresh: {}", cartDto, refresh);
    cartDto.getItems().forEach(itemDto -> enrichItem(itemDto, cartDto.getCurrencyId(), refresh));

    cartDto.getItems()
        .stream()
        .filter(Objects::nonNull)
        .filter(ItemDto::isOrderable)
        .map(ItemDto::getPrice)
        .filter(Objects::nonNull)
        .reduce(BigDecimal::add)
        .ifPresent(cartDto::setTotal);

    var isCartOrderable = cartDto.getItems()
        .stream()
        .allMatch(ItemDto::isOrderable);
    cartDto.setOrderable(isCartOrderable);

    if (!cartDto.isOrderable()) {
      cartDto.addMessage(new MessageDto(
          MessageLevel.ERROR,
          messageSource.getMessage("cart.notOrderable", null, LocaleContextHolder.getLocale())));
    }
    return cartDto;
  }

  protected ItemDto enrichItem(ItemDto itemDto, String currencyId, final List<String> refresh) {
    boolean orderable = true;

    //retrieve Price for the item
    if (refresh.isEmpty() || refresh.contains("price")) {
      List<PriceDto> prices = getPrice(itemDto.getArticleId(), currencyId);
      if (!prices.isEmpty()) {
        PriceDto itemPrice = prices.get(0);
        itemDto.setUnitPrice(itemPrice.getUnitPrice());

        //set item Quantity to min orderable quantity if it is less that the specified value
        if (itemDto.getQuantity().compareTo(itemPrice.getMinQuantity()) < 0) {
          itemDto.setQuantity(itemPrice.getMinQuantity());
        }

        itemDto.setPrice(itemDto.getQuantity().multiply(itemDto.getUnitPrice()));

      }
      //Price does not exist, item cannot be ordered
      else {
        orderable = false;
        itemDto.addMessage(new MessageDto(MessageLevel.ERROR,
            messageSource.getMessage("item.price.notExist", null,
                LocaleContextHolder.getLocale())));
      }
    }

    //retrieve Availability for the item
    if (refresh.isEmpty() || refresh.contains("availability")) {
      List<AvailabilityDto> availabilities = getStock(itemDto.getArticleId());
      if (!availabilities.isEmpty()) {
        AvailabilityDto stockInfo = availabilities.get(0);
        itemDto.setAvailability(stockInfo);

        if (stockInfo.getStatus() == AvailabilityStatus.NOT_AVAILABLE) {
          String messageCode = "item.availability.notAvailable";

          //TODO Add message that you can order now amount available in stock and have to wait for replenishment time to get the rest
          //Not more than available quantity can be ordered
          if (itemDto.getQuantity().compareTo(stockInfo.getQuantity()) > 0) {
            itemDto.setQuantity(stockInfo.getQuantity());
            messageCode = "item.availability.partiallyAvailable";
            //if the Unit price exists, total price needs to be adjusted
            if (itemDto.getUnitPrice() != null) {
              itemDto.setPrice(itemDto.getQuantity().multiply(itemDto.getUnitPrice()));
            }
          }
          itemDto.addMessage(new MessageDto(MessageLevel.WARNING,
              messageSource.getMessage(messageCode, null, LocaleContextHolder.getLocale())));
        }
      }
      //Availability does not exist, item can be ordered
      else {
        itemDto.addMessage(new MessageDto(MessageLevel.WARNING,
            messageSource.getMessage("item.availability.notExist", null,
                LocaleContextHolder.getLocale())));
      }
    }

    //retrieve Catalog information for the item
    if (refresh.isEmpty() || refresh.contains("catalog")) {
      EnrichedProductDto catalogItemDto = getCatalogItem(
          itemDto.getArticleId(), currencyId, Collections.singletonList("catalog"));
      if (catalogItemDto != null) {
        itemDto.setName(catalogItemDto.getName());
        itemDto.setProduct(catalogItemDto);
      }
    }

    if (itemDto.getUnitPrice() == null || itemDto.getPrice() == null) {
      orderable = false;
    }

    itemDto.setOrderable(orderable);
    return itemDto;
  }


  @Override
  public ResponseEntity<String> getCarts(final Pageable pageable) {

    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts")
        .queryParam("page", pageable.getPageNumber())
        .queryParam("size", pageable.getPageSize());
    if (pageable.getSort() != null && !Sort.unsorted().equals(pageable.getSort())) {
      uriComponentsBuilder = uriComponentsBuilder.queryParam("sort", pageable.getSort());
    }
    URI targetUrl = uriComponentsBuilder
        .build()
        .toUri();

    ResponseEntity<String> responseEntity = restTemplate.exchange(targetUrl, HttpMethod.GET,
        new HttpEntity<>(pageable), new ParameterizedTypeReference<>() {
        });
    LOG.debug("{}, {}", responseEntity.getStatusCode(), responseEntity.getBody());

    return responseEntity;
  }

  @Override
  public ResponseEntity<CartDto> updateCart(final String cartId,
      final shop.velox.cart.api.dto.CartDto cartServiceDto, final String userId) {
    ResponseEntity<CartDto> cartEntity = getCart(cartId, userId, List.of());

    if (!cartEntity.getStatusCode().is2xxSuccessful() || cartEntity.getBody() == null) {
      return cartEntity;
    }
    CartDto originalCart = cartEntity.getBody();

    if (StringUtils.isNotBlank(cartServiceDto.getCurrencyId()) && !cartServiceDto.getCurrencyId()
        .equals(originalCart.getCurrencyId())) {
      verifyCurrency(cartServiceDto);
      return updateCurrency(originalCart, cartServiceDto.getCurrencyId());
    } else {
      return cartEntity;
    }
  }

  private ResponseEntity<CartDto> updateCurrency(CartDto cart, String currencyId) {
    cart.getItems().forEach(item -> {
      getPrice(item.getArticleId(), currencyId).stream()
          .filter(price -> currencyId.equals(price.getCurrency().getId()))
          .filter(price -> price.getMinQuantity().compareTo(item.getQuantity()) <= 0)
          .min(Comparator.nullsLast(Comparator.comparing(PriceDto::getUnitPrice)))
          .ifPresent(price -> updatePrice(cart, item, price));
    });

    cart.setCurrencyId(currencyId);
    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cart.getId())
        .build()
        .toUri();
    ResponseEntity<CartDto> cartResponseEntity = restTemplate
        .exchange(uri, HttpMethod.PATCH, new HttpEntity<>(cart), CartDto.class);
    var cartDto = cartResponseEntity.getBody();

    if (cartResponseEntity.getStatusCode().is2xxSuccessful()) {
      return ResponseEntity.status(cartResponseEntity.getStatusCode())
          .body(enrichCart(cartDto, Collections.emptyList()));
    } else {
      return cartResponseEntity;
    }
  }

  private void updatePrice(CartDto cart, ItemDto item, PriceDto price) {
    item.setUnitPrice(price.getUnitPrice());
    item.setPrice(item.getQuantity().multiply(price.getUnitPrice()));

    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cart.getId())
        .path("/items")
        .build()
        .toUri();
    ResponseEntity<CartDto> updatedCartEntity = restTemplate
        .exchange(uri, HttpMethod.PATCH, new HttpEntity<>(item), CartDto.class);
  }

  @Override
  public ResponseEntity<ItemDto> getItem(final String cartId, final String itemId,
      final List<String> refresh) {
    URI uriCart = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cartId)
        .build()
        .toUri();
    ResponseEntity<CartDto> cartDto = restTemplate.getForEntity(uriCart, CartDto.class);

    if (!cartDto.getStatusCode().is2xxSuccessful() || cartDto.getBody() == null) {
      return ResponseEntity.status(cartDto.getStatusCode()).build();
    }

    ResponseEntity<ItemDto> itemDtoFromCartEntity = getItem(cartId, itemId);

    if (itemDtoFromCartEntity.getBody() == null) {
      return new ResponseEntity<>(itemDtoFromCartEntity.getStatusCode());
    }

    return ResponseEntity.status(itemDtoFromCartEntity.getStatusCode())
        .body(enrichItem(itemDtoFromCartEntity.getBody(), cartDto.getBody().getCurrencyId(),
            refresh));
  }

  // TODO Refactor this should return a shop.velox.cart.api.dto.ItemDto
  private ResponseEntity<ItemDto> getItem(final String cartId,
      final String itemId) {
    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cartId)
        .path("/items/")
        .path(itemId)
        .build()
        .toUri();

    return getItem(uri);
  }

  private ResponseEntity<ItemDto> getItem(URI uri) {
    ResponseEntity<ItemDto> itemDtoResponseEntity = restTemplate.getForEntity(
        uri, ItemDto.class);
    if (!itemDtoResponseEntity.getStatusCode().is2xxSuccessful()) {
      return itemDtoResponseEntity;
    }
    return ResponseEntity.status(itemDtoResponseEntity.getStatusCode())
        .body(itemDtoResponseEntity.getBody());
  }

  public static URI getLocationHeaderFromResponseEntity(ResponseEntity<?> responseEntity,
      Supplier<String> errorMessageProvider) {
    try {
      String location = responseEntity.getHeaders()
          .get(HttpHeaders.LOCATION)
          .get(0);
      return new URI(location);
    } catch (URISyntaxException | NullPointerException | IndexOutOfBoundsException e) {
      String errorMessage = errorMessageProvider.get();
      LOG.error(errorMessage, e);
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  @Override
  public ResponseEntity<CartDto> addItem(final String cartId,
      final shop.velox.cart.api.dto.ItemDto itemServiceDto,
      final List<String> refresh) {
    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cartId)
        .path("/items")
        .build()
        .toUri();
    ResponseEntity<CartDto> cartResponseEntity = restTemplate.postForEntity(uri, itemServiceDto,
        CartDto.class);

    if (HttpStatus.CONFLICT.equals(cartResponseEntity.getStatusCode())) {

      Supplier<String> errorMessageProvider = () -> MessageFormat.format(
          "cannot read URI of existing item with cartId: {0} and articleId: {1}", cartId,
          itemServiceDto.getArticleId());

      URI existingItemUri = getLocationHeaderFromResponseEntity(cartResponseEntity,
          errorMessageProvider);

      ResponseEntity<ItemDto> alreadyExistingItemEntity = getItem(existingItemUri);

      if (!HttpStatus.OK.equals(alreadyExistingItemEntity.getStatusCode())) {
        LOG.error("Cart: {} item with articleId {} already exist or not?", cartId,
            itemServiceDto.getArticleId());
        throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
      }

      ItemDto alreadyExistingItem = alreadyExistingItemEntity.getBody();
      BigDecimal alreadyExistingQuantity = alreadyExistingItem.getQuantity();
      BigDecimal quantityToAdd = itemServiceDto.getQuantity();
      BigDecimal newQuantity = quantityToAdd.add(alreadyExistingQuantity);
      LOG.info("On Cart: {} item with articleId: {} had quantity: {}. Increasing it by {} to {}",
          cartId, itemServiceDto.getArticleId(), alreadyExistingQuantity, quantityToAdd,
          newQuantity);

      BigDecimal newTotalPrice = firstNonNull(itemServiceDto.getUnitPrice(),
          BigDecimal.ZERO) // FIXME in https://gitlab.com/velox-shop/cart-orchestration/-/issues/17
          .multiply(newQuantity)
          .setScale(2, RoundingMode.DOWN);
      shop.velox.cart.api.dto.ItemDto updatedItemDto =
          shop.velox.cart.api.dto.ItemDto.builder()
              .id(alreadyExistingItem.getId())
              .articleId(itemServiceDto.getArticleId())
              .quantity(newQuantity)
              .name(itemServiceDto.getName())
              .unitPrice(itemServiceDto.getUnitPrice())
              .totalPrice(newTotalPrice)
              .build();

      return updateItem(cartId, updatedItemDto);
    }

    if (!HttpStatus.CREATED.equals(cartResponseEntity.getStatusCode())) {
      return cartResponseEntity;
    }

    return ResponseEntity.status(cartResponseEntity.getStatusCode())
        .body(enrichCart(cartResponseEntity.getBody(), refresh));
  }

  @Override
  public ResponseEntity<CartDto> updateItem(final String cartId,
      final shop.velox.cart.api.dto.ItemDto itemServiceDto) {
    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cartId)
        .path("/items")
        .build()
        .toUri();
    ResponseEntity<CartDto> cartResponseEntity = restTemplate
        .exchange(uri, HttpMethod.PATCH, new HttpEntity<>(itemServiceDto), CartDto.class);
    if (HttpStatus.NOT_FOUND.equals(cartResponseEntity.getStatusCode())) {
      return cartResponseEntity;
    }
    return ResponseEntity.status(cartResponseEntity.getStatusCode())
        .body(enrichCart(cartResponseEntity.getBody(), Collections.emptyList()));
  }

  @Override
  public ResponseEntity<CartDto> removeItem(final String cartId, final String itemId) {
    URI uri = UriComponentsBuilder.fromUriString(cartUrl)
        .path("/carts/")
        .path(cartId)
        .path("/items/")
        .path(itemId)
        .build()
        .toUri();
    ResponseEntity<CartDto> responseEntity = restTemplate
        .exchange(uri, HttpMethod.DELETE, null, CartDto.class);
    LOG.debug("delete responseEntity: {}", responseEntity);
    return responseEntity;
  }

  protected List<AvailabilityDto> getStock(String articleId) {
    URI uri = UriComponentsBuilder.fromUriString(availabilityUrl)
        .path("/availabilities")
        .queryParam("articleId", articleId)
        .build()
        .toUri();

    ParameterizedTypeReference<RestResponsePage<AvailabilityDto>> responseType = new ParameterizedTypeReference<>() {
    };
    return restTemplate.exchange(uri, HttpMethod.GET, null/*httpEntity*/, responseType).getBody()
        .getContent();
  }

  protected List<PriceDto> getPrice(String articleId, String currencyId) {
    URI uri = UriComponentsBuilder.fromUriString(priceUrl)
        .path("/prices")
        .queryParam("statusFilter", "ACTIVE")
        .queryParam("articleId", articleId)
        .queryParam("currencyId", currencyId)
        .build()
        .toUri();
    ParameterizedTypeReference<RestResponsePage<PriceDto>> responseType = new ParameterizedTypeReference<>() {
    };
    return restTemplate.exchange(uri, HttpMethod.GET, null/*httpEntity*/, responseType).getBody()
        .getContent();
  }

  protected EnrichedProductDto getCatalogItem(String articleId, String currencyId,
      List<String> refresh) {
    URI uri = UriComponentsBuilder.fromUriString(catalogUrl)
        .path("/catalogs/" + articleId)
        .queryParam("currencyId", currencyId)
        .build()
        .toUri();
    ResponseEntity<EnrichedProductDto> responseEntity = restTemplate.exchange(
        uri, HttpMethod.GET, null/*httpEntity*/, EnrichedProductDto.class);
    if (responseEntity.getStatusCode().isError()) {
      LOG.error("getCatalogItem for articleId: {}, currencyId: {} got {}", articleId, currencyId,
          responseEntity.getStatusCodeValue());
    }
    return responseEntity.getBody();
  }

  private void verifyCurrency(shop.velox.cart.api.dto.CartDto cartServiceDto) {
    if (StringUtils.isBlank(cartServiceDto.getCurrencyId())) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY, "Currency not available");
    }

    URI uri = UriComponentsBuilder.fromUriString(priceUrl)
        .pathSegment("currencies", cartServiceDto.getCurrencyId())
        .build()
        .toUri();

    ResponseEntity<CurrencyDto> currencyResponseEntity = restTemplate
        .getForEntity(uri, CurrencyDto.class);

    if (!currencyResponseEntity.getStatusCode().is2xxSuccessful()) {
      throw new ResponseStatusException(HttpStatus.UNPROCESSABLE_ENTITY,
          "Could not find currency for ID " + cartServiceDto.getCurrencyId());
    }
  }

}
