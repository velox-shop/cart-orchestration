package shop.velox.cart_orchestration.converter.impl;


import org.mapstruct.Mapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.commons.converter.Converter;

@Mapper(uses = ItemConverter.class)
public interface CartConverter extends Converter<CartDto, shop.velox.cart.api.dto.CartDto> {

}
