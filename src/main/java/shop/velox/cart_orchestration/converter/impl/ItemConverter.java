package shop.velox.cart_orchestration.converter.impl;


import org.mapstruct.Mapper;
import shop.velox.commons.converter.Converter;

@Mapper
public interface ItemConverter extends Converter<shop.velox.cart_orchestration.api.dto.ItemDto, shop.velox.cart.api.dto.ItemDto> {

}
