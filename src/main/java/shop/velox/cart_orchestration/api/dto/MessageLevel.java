package shop.velox.cart_orchestration.api.dto;

public enum MessageLevel {
  INFO,
  WARNING,
  ERROR
}
