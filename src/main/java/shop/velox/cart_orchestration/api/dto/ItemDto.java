package shop.velox.cart_orchestration.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import lombok.Data;
import shop.velox.availability.api.dto.AvailabilityDto;
import shop.velox.catalog.dto.ProductDto;

@JsonInclude(Include.NON_NULL)
@Data
public class ItemDto {

  @Schema(description = "Unique identifier of the Item.", example = "2e5c9ba8-956e-476b-816a-49ee128a40c9")
  private String id;

  @Schema(description = "Unique identifier of the Item.", example = "article1", required = true)
  private String articleId;

  @Schema(description = "Name of the Item.", example = "notebook")
  @JsonInclude(Include.ALWAYS)
  private String name;

  @Schema(description = "Quantity of articles of the Item.", example = "10", required = true)
  private BigDecimal quantity;

  @Schema(description = "Total price of the Item.", example = "100.10")
  @JsonInclude(Include.ALWAYS)
  private BigDecimal price;

  @Schema(description = "Unit Price of the Item.", example = "10.01")
  @JsonInclude(Include.ALWAYS)
  private BigDecimal unitPrice;

  @Schema(description = "Availability of the Item.", example = "100")
  @JsonInclude(Include.ALWAYS)
  private AvailabilityDto availability;

  @Schema(description = "Catalog information of an item.")
  @JsonInclude(Include.ALWAYS)
  private ProductDto product;

  @Schema(description = "Indicates whether an Item can be ordered", example = "true")
  @JsonInclude(Include.ALWAYS)
  private boolean orderable;

  @Schema(description = "The messages on the item", example = "{level=ERROR, message=Price for this item does not exist}")
  private List<MessageDto> messages = new ArrayList<>();

  public void addMessage(MessageDto message) {
    this.messages.add(message);
  }

}
