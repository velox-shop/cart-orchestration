package shop.velox.cart_orchestration.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class MessageDto {

  @Schema(description = "Message level", example = "INFO")
  private MessageLevel level;

  @Schema(description = "Message content", example = "Price for this item does not exist")
  private String message;

  public MessageDto() {
    super();
  }

  public MessageDto(MessageLevel level, String message) {
    this.level = level;
    this.message = message;
  }

  public MessageDto(String message) {
    this.level = MessageLevel.INFO;
    this.message = message;
  }

  public MessageLevel getLevel() {
    return level;
  }

  public void setLevel(MessageLevel level) {
    this.level = level;
  }

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

}
