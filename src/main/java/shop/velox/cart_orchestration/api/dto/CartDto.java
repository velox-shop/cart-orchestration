package shop.velox.cart_orchestration.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import io.swagger.v3.oas.annotations.media.Schema;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import jakarta.validation.constraints.NotNull;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@JsonInclude(Include.NON_NULL)
public class CartDto {

  @Schema(description = "Unique identifier of the Cart.", example = "4b7e6hjA-107b-476b-816a-51hh256a30d1")
  private String id;

  @Schema(description = "Unique identifier of the User.", example = "4b7e6hjA-107b-476b-816a-51hh256a30d1")
  private String userId;

  @Schema(description = "List of the Items in the Cart.")
  private List<ItemDto> items;

  @Schema(description = "Total value of all Items in the Cart.", example = "1050.75")
  @JsonInclude(Include.ALWAYS)
  private BigDecimal total;

  @Schema(description = "Indicates whether all content in the Cart can be ordered and proceeded to checkout", example = "true")
  private boolean orderable;

  @Schema(description = "The currency of the cart.", example = "25dbb90c-04c5-4073-a972-b3381745c951")
  @NotNull
  private String currencyId;

  @Schema(description = "The messages on the cart.", example = "{level=ERROR, message=Certain items in the cart cannot be ordered}")
  private List<MessageDto> messages;

  public CartDto(String id, List<ItemDto> items, BigDecimal total, boolean orderable, List<MessageDto> messages) {
    setId(id);
    setItems(items);
    setTotal(total);
    setOrderable(orderable);
    this.messages = messages;
  }

  public CartDto(String id, List<ItemDto> items, BigDecimal total, boolean orderable) { this(id, items, total, orderable, new ArrayList<>()); }

  public CartDto(String id, List<ItemDto> items) {
    this(id, items, BigDecimal.ZERO, false);
  }

  public CartDto(String id) {
    this(id, new ArrayList<>(), BigDecimal.ZERO, false);
  }

  public CartDto(){ this.messages = new ArrayList<>(); }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public List<ItemDto> getItems() {
    return items;
  }

  public void setItems(List<ItemDto> items) { this.items = ListUtils.defaultIfNull(items, new ArrayList<>()); }

  public BigDecimal getTotal() {
    return total;
  }

  public void setTotal(BigDecimal total) {
    this.total = total;
  }

  public boolean isOrderable() { return orderable; }

  public void setOrderable(boolean orderable) { this.orderable = orderable; }

  public String getCurrencyId() {
    return currencyId;
  }

  public void setCurrencyId(String currencyId) {
    this.currencyId = currencyId;
  }

  public List<MessageDto> getMessages() { return messages; }

  public void setMessages(List<MessageDto> messages) { this.messages = messages; }

  public void addMessage(MessageDto message) { this.messages.add(message); }

  @Override
  public String toString() {
    return ReflectionToStringBuilder.toString(this, ToStringStyle.JSON_STYLE);
  }

}
