package shop.velox.cart_orchestration.api.controller.impl;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.cart_orchestration.api.controller.ItemController;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.cart_orchestration.api.dto.ItemDto;
import shop.velox.cart_orchestration.converter.impl.ItemConverter;
import shop.velox.cart_orchestration.service.CartOrchestrationService;


@RestController
@RequiredArgsConstructor
public class ItemControllerImpl implements ItemController {

  private final CartOrchestrationService cartOrchestrationService;
  private final ItemConverter itemConverter;

  @Override
  public ResponseEntity<ItemDto> getItem(String cartId, String itemId, List<String> refresh) {
    return cartOrchestrationService.getItem(cartId, itemId, refresh);
  }

  @Override
  public ResponseEntity<CartDto> addItem(String cartId, ItemDto item, List<String> refresh) {
    shop.velox.cart.api.dto.ItemDto itemServiceDto = itemConverter.convertEntityToDto(item);
    return cartOrchestrationService.addItem(cartId, itemServiceDto ,refresh);
  }

  @Override
  public ResponseEntity<CartDto> updateItem(String cartId, ItemDto item) {
    shop.velox.cart.api.dto.ItemDto itemServiceDto = itemConverter.convertEntityToDto(item);
    return cartOrchestrationService.updateItem(cartId, itemServiceDto);
  }

  @Override
  public ResponseEntity<CartDto> removeItem(String id, String itemId) {
    return cartOrchestrationService.removeItem(id, itemId);
  }

}
