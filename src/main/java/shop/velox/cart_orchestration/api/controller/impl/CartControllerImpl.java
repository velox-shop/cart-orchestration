package shop.velox.cart_orchestration.api.controller.impl;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.cart_orchestration.api.controller.CartController;
import shop.velox.cart_orchestration.api.dto.CartDto;
import shop.velox.cart_orchestration.converter.impl.CartConverter;
import shop.velox.cart_orchestration.service.CartOrchestrationService;


@RestController
@RequiredArgsConstructor
public class CartControllerImpl implements CartController {

  private final CartOrchestrationService cartOrchestrationService;
  private final CartConverter cartConverter;

  @Override
  public ResponseEntity<CartDto> createCart(final CartDto cart) {
    shop.velox.cart.api.dto.CartDto cartServiceDto = cartConverter.convertEntityToDto(cart);

    return cartOrchestrationService.createCart(cartServiceDto);
  }

  @Override
  public ResponseEntity<CartDto> getCart(final String id, final String userId,
      final List<String> refresh) {
    return cartOrchestrationService.getCart(id, userId, refresh);
  }

  @Override
  public ResponseEntity<Void> removeCart(final String cartId) {
    return cartOrchestrationService.removeCart(cartId);
  }

  @Override
  public ResponseEntity<String> getCarts(final Pageable pageable) {
    return cartOrchestrationService.getCarts(pageable);
  }

  @Override
  public ResponseEntity<CartDto> updateCart(String cartId, CartDto cart, String userId) {
    shop.velox.cart.api.dto.CartDto cartServiceDto = cartConverter.convertEntityToDto(cart);
    return cartOrchestrationService.updateCart(cartId, cartServiceDto, userId);
  }

}
