# Work locally

Build Service

```
./gradlew clean build --refresh-dependencies
```

Execute dependencies with Docker Compose and this service with Gradle:

```
docker-compose --file docker-compose.dependencies.yml up
./gradlew bootRun
```

Execute everything with Docker Compose

```
docker-compose --file docker-compose.dependencies.yml --file docker-compose.this.yml up --build --force-recreate --remove-orphans --renew-anon-volumes
```

Test

```
for file in src/test/*postman_collection.json; do newman run $file --environment src/test/*-local.postman_environment.json --reporters cli,html --reporter-html-export "newman-results-$(basename $file .json).html"; done
```

## API documentation

Online API Documentation is at:

- HTML: http://localhost:8443/cart-orchestration/v1/swagger-ui.html
- JSON: http://localhost:8443/cart-orchestration/v1/api-docs

Offline API Documentation is at https://velox-shop.gitlab.io/cart-orchestration/

| Version     | Description                                                                                                                  |
|-------------|------------------------------------------------------------------------------------------------------------------------------|
| 3.0.1       | Add MessageDto and MessageLevel to be published to GitLab Maven repository, along with already published CartDto and ItemDto |
| 3.0.0       | BREAKING CHANGE: Change structure of ItemDto (whole product info from new catalog is saved in the item object)               |
| 2.1.2       | Add messages to CartDto and ItemDto                                                                                          |
| ...         | ...                                                                                                                          |
| 1.0.0 (1.0) | Base image                                                                                                                   |
